from datetime import datetime
import sys
from pathlib import Path
two_up = Path(__file__).resolve().parents[1]
sys.path.append(str(two_up))


class UserAccount:
    EPOCH_TIMESTAMP = 0
    UNIT_RATE = 0.8

    def __init__(self):
        self.services = []
        self.payment_dates = []
        self.calculation_history_service = None

    def recalculate_balance(self):
        for service in self.services:
            self.recalculate_service(service)

    def recalculate_service(self, service):
        history = self.get_history_from_service(service)
        self.pay_tariff(history, self.get_highest_tariff(service))

    def get_history_from_service(self, service):
        return self.calculation_history_service.retrieve_history(service)

    def calculate_unapplied(self, tariff, fees):
        sum = 0
        for d in fees:
            for date in d:
                if date > self.get_last_calculation_date():
                    sum += self.calculate_fee(tariff, d[date])
        return sum

    def get_last_calculation_date(self):
        latest = UserAccount.EPOCH_TIMESTAMP
        for p in self.payment_dates:
            t = (p - datetime(1970,1,1)).total_seconds()
            latest = max(t, latest)
        return datetime.fromtimestamp(latest)

    def pay_tariff(self, history, highest_tariff):
        history.apply_recalculation(highest_tariff, UserAccount.UNIT_RATE)
        self.balance.update_balance(highest_tariff)

    def get_highest_tariff(self, service):
        tariffs = service.get_tariffs()
        highest_tariff = 0
        for tariff in tariffs:
            unapplied_tariff = self.get_unapplied_tariff(tariff, service)
            highest_tariff = max(highest_tariff, unapplied_tariff)
        return highest_tariff

    def get_unapplied_tariff(self, tariff, service):
        history = self.get_history_from_service(service)
        all_history_fees = history.get_all_fees(tariff, service)
        return self.calculate_unapplied(tariff, all_history_fees)

    def calculate_fee(self, tariff, fee):
        return fee * self.get_rate(tariff) + tariff.get_additional_fee()
    
    def get_rate(self, tariff):
        is_unit_based = tariff.get_type().is_unit_based()
        return UserAccount.UNIT_RATE if is_unit_based else 1

    def set_calculation_history_service(self, calculation_history_service):
        self.calculation_history_service = calculation_history_service

    def set_services(self, services):
        self.services = services

    def set_balance(self, balance):
        self.balance = balance

    def set_payment_dates(self, payment_dates):
        self.payment_dates = payment_dates

