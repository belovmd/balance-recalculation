import unittest
from datetime import datetime


class HistoryStub(unittest.TestCase):
    DELTA = 0.0001
    UNIT_RATE = 0.8

    def __init__(self, uncalculated_fees):
        super().__init__()
        self.applied_sum = 0
        self.fees = [{datetime(2001, 3, 28): 100},
                     {datetime(2001, 4, 18): 150}]

        for fee in uncalculated_fees:
            self.fees.append(fee)

    def get_all_fees(self, tariff, service):
        return self.fees

    def apply_recalculation(self, value, unit_rate):
        self.applied_sum = value

    def verify_applied_sum(self, expected_sum):
        self.assertEqual(expected_sum, self.applied_sum, HistoryStub.DELTA)
